package com.example.hamimnizar.resikapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.hamimnizar.resikapp.fragments.MissionFragment;
import com.example.hamimnizar.resikapp.fragments.NewsFragment;

import java.util.List;

public class AdapterMission extends RecyclerView.Adapter<AdapterMission.ListViewHolder> {


    private List<ModelM> modelms;
    MissionFragment context;

    public AdapterMission(MissionFragment context, List<ModelM> modelms) {
        this.context = context;
        this.modelms = modelms;

    }


//    public AdapterNewsH(List<Model> models, NewsFragment newsFragment) {
//
//    }

    @Override
    public ListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_recyclerviewvm, parent, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ListViewHolder holder, int position) {
        holder.txpoint.setText(modelms.get(position).getPoint());
        holder.txexp.setText(modelms.get(position).getExp());
        holder.txtitle.setText(modelms.get(position).getTitle());
        Glide.with(context).load(modelms.get(position).getImage()).into(holder.img);

        holder.crd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), DetailMissionActivity.class);
                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return (modelms != null) ? modelms.size() : 0;
    }

    public class ListViewHolder extends RecyclerView.ViewHolder{
        private TextView txtitle, txpoint, txexp;
        private ImageView img;
        private CardView crd;

        public ListViewHolder(View itemView) {
            super(itemView);
            txexp = (TextView) itemView.findViewById(R.id.Exp);
            txpoint = (TextView) itemView.findViewById(R.id.Point);
            txtitle = (TextView) itemView.findViewById(R.id.Title);
            img = (ImageView) itemView.findViewById(R.id.imgm);
            crd = (CardView) itemView.findViewById(R.id.cardvm);
        }
    }
}
