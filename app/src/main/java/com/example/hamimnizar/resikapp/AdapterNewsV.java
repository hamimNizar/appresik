package com.example.hamimnizar.resikapp;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.hamimnizar.resikapp.fragments.NewsFragment;

import java.util.List;

public class AdapterNewsV extends RecyclerView.Adapter<AdapterNewsV.ListViewHolder> {


    private List<Model> models;
    private NewsFragment context;

    public AdapterNewsV(NewsFragment context, List<Model> models) {
        this.context = context;
        this.models = models;

    }


    @Override
    public ListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_recyclerviewv, parent, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ListViewHolder holder, int position) {
        holder.txttitle.setText(models.get(position).getTitle());
        Glide.with(context).load(models.get(position).getImage()).into(holder.img);
    }

    @Override
    public int getItemCount() {
        return (models != null) ? models.size() : 0;
    }

    public class ListViewHolder extends RecyclerView.ViewHolder{
        private TextView txttitle;
        private ImageView img;

        public ListViewHolder(View itemView) {
            super(itemView);
            txttitle = (TextView) itemView.findViewById(R.id.titlev);
            img = (ImageView) itemView.findViewById(R.id.imgv);
        }
    }
}