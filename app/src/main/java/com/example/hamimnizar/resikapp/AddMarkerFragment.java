package com.example.hamimnizar.resikapp;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;


public class AddMarkerFragment extends Fragment {

    private ImageView imgmarker;
    private EditText namamarker;
    private TextView alamat;
    private RadioGroup jenistempat, status, tipesetor;
    private RadioButton rbjenis, rbtipe, rbstatus;
    private Button btnmarker;

    private static final int CAMERA_REQUEST = 1888;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_add_marker, container, false);

        imgmarker = view.findViewById(R.id.img_addmarker);
        namamarker = view.findViewById(R.id.et_addnama);
        alamat = view.findViewById(R.id.tv_addalamat);

        jenistempat = (RadioGroup) view.findViewById(R.id.jenis);
        int selectedJenis = jenistempat.getCheckedRadioButtonId();
        rbjenis = (RadioButton)view.findViewById(selectedJenis);
        rbjenis.getText();

        status = (RadioGroup) view.findViewById(R.id.status);
        int selectedStatus = status.getCheckedRadioButtonId();
        rbstatus = (RadioButton)view.findViewById(selectedStatus);
        rbstatus.getText();

        tipesetor = (RadioGroup) view.findViewById(R.id.setor);
        int selectedTipesetor = tipesetor.getCheckedRadioButtonId();
        rbtipe = (RadioButton)view.findViewById(selectedTipesetor);
        rbtipe.getText();

        btnmarker = view.findViewById(R.id.btn_add_marker);
        btnmarker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"Lokasi berhasil ditambahkan",Toast.LENGTH_LONG).show();

                Intent intentnya = new Intent(getActivity(), MainActivity.class);
                startActivity(intentnya);
            }
        });


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, 0);
            }
            else{
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        }

        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_PERMISSION_CODE)
        {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                Toast.makeText(getActivity(), "camera permission granted", Toast.LENGTH_LONG).show();
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
            else
            {
                Toast.makeText(getActivity(), "camera permission denied", Toast.LENGTH_LONG).show();
            }
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK)
        {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            photo = Bitmap.createScaledBitmap(photo, 300,300, false);
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            photo.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

            imgmarker.setImageBitmap(photo);
        }
    }


}
