package com.example.hamimnizar.resikapp;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;

import com.example.hamimnizar.resikapp.adapter.SlideMainAdapter;
import com.example.hamimnizar.resikapp.fragments.NewsFragment;
import com.example.hamimnizar.resikapp.model.Berita;
import com.example.hamimnizar.resikapp.sampah.ListPlaceActivity;

import java.util.ArrayList;


public class HomeFragment extends Fragment {

    private SearchView searchView;
    private SlideMainAdapter slideMainAdapter;
    private RecyclerView recyclerSlide;
    private ArrayList<Berita> beritaArrayList;
    private ImageView imgbanksampah, imgtpa, imgts,imgberita, imgmisi, imgtantangan;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_home, container, false);

        searchView = view.findViewById(R.id.search);

        imgbanksampah = view.findViewById(R.id.img_bs);
        imgtpa = view.findViewById(R.id.img_tpa);
        imgts = view.findViewById(R.id.img_ts);
        imgberita = view.findViewById(R.id.img_berita);
        imgmisi = view.findViewById(R.id.img_misi);
        imgtantangan = view.findViewById(R.id.img_tantangan);


        imgbanksampah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentbs = new Intent(getContext(), ListPlaceActivity.class);
                startActivity(intentbs);
            }
        });
        imgtpa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intenttpa = new Intent(getContext(), ListPlaceActivity.class);
                startActivity(intenttpa);
            }
        });
        imgts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentts = new Intent(getContext(), ListPlaceActivity.class);
                startActivity(intentts);
            }
        });
        imgberita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentberita = new Intent(getContext(), NewsFragment.class);
                startActivity(intentberita);
            }
        });

        imgmisi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //intent ke misi
            }
        });

        imgtantangan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //intent ke tantangan
            }
        });

        addDataSlide();

        recyclerSlide = view.findViewById(R.id.recyclerview_slide);
        slideMainAdapter = new SlideMainAdapter(beritaArrayList, getContext());

        RecyclerView.LayoutManager layoutManagerSlide = new LinearLayoutManager(getContext());
        recyclerSlide.setLayoutManager(layoutManagerSlide);

        recyclerSlide.setAdapter(slideMainAdapter);

        ((LinearLayoutManager) layoutManagerSlide).setOrientation(LinearLayoutManager.HORIZONTAL);
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(recyclerSlide);


        return view;

    }

    void addDataSlide() {
        beritaArrayList = new ArrayList<>();
        beritaArrayList.add(new Berita("Ini Judul Berita","28 Oktober 2019","isi nya masih dummy yaaaaaaa",R.drawable.logo));
        beritaArrayList.add(new Berita("Berita Ini Judul","28 Oktober 2019","isi nya masih dummy yaaaaaaa",R.drawable.logo));
        beritaArrayList.add(new Berita("Judul Ini Berita","28 Oktober 2019","isi nya masih dummy yaaaaaaa",R.drawable.logo));
        beritaArrayList.add(new Berita("Judul Berita Ini","28 Oktober 2019","isi nya masih dummy yaaaaaaa",R.drawable.logo));
    }

}
