package com.example.hamimnizar.resikapp;

import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.hamimnizar.resikapp.fragments.MapsFragment;
import com.example.hamimnizar.resikapp.fragments.MissionFragment;
import com.example.hamimnizar.resikapp.fragments.NewsFragment;
import com.example.hamimnizar.resikapp.fragments.ProfileFragment;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    FrameLayout frameLayout;
    BottomNavigationView navView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        frameLayout = findViewById(R.id.fl_container);
        navView = findViewById(R.id.nav_view);

        loadFragment(new HomeFragment());

        navView.setOnNavigationItemSelectedListener(this);

    }

    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fl_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;

        switch (item.getItemId()){
            case R.id.nav_home:
                fragment = new HomeFragment();

                break;
            case R.id.nav_kontribusi:
                fragment = new AddMarkerFragment();

                break;
            case R.id.nav_profile:
                fragment = new ProfileFragment();
                break;

        }
        return loadFragment(fragment);
    }
}
