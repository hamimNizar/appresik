package com.example.hamimnizar.resikapp;

public class ModelM {

    private int image, bg;
    private String title, exp, point, status;

    public ModelM(int image, String title, String exp, String point, String status) {
        this.image = image;
        this.title = title;
        this.exp = exp;
        this.point = point;
        this.status = status;
    }


    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getExp() {
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
