package com.example.hamimnizar.resikapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.hamimnizar.resikapp.R;
import com.example.hamimnizar.resikapp.model.Berita;

import java.util.ArrayList;
import java.util.List;

public class SlideMainAdapter extends RecyclerView.Adapter<SlideMainAdapter.SlideViewHolder> {
    private ArrayList<Berita> beritas;
    private Context context;

    public SlideMainAdapter(ArrayList<Berita> beritas, Context context) {
        this.beritas = beritas;
        this.context = context;
    }

    @NonNull
    @Override
    public SlideViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_slide_home, viewGroup,false);
        return new SlideViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SlideViewHolder slideViewHolder, int position) {
        slideViewHolder.judul.setText(beritas.get(position).getJudul());
        slideViewHolder.tanggal.setText(beritas.get(position).getTanggal());
        slideViewHolder.isi.setText(beritas.get(position).getIsi());
        Glide.with(context).load(beritas.get(position).getImage()).into(slideViewHolder.gambar);

    }

    @Override
    public int getItemCount() {
        return ( beritas != null ? beritas.size() : 0);
    }

    public class SlideViewHolder extends RecyclerView.ViewHolder {
        private TextView judul, isi, tanggal;
        private ImageView gambar;
        private CardView cardViewSlide;

        public SlideViewHolder(@NonNull View itemView) {
            super(itemView);

            judul = itemView.findViewById(R.id.judul_slideberita);
            tanggal = itemView.findViewById(R.id.tgl_slideberita);
            isi = itemView.findViewById(R.id.isi_berita);
            gambar = itemView.findViewById(R.id.img_slideBerita);

            cardViewSlide = itemView.findViewById(R.id.card_slide_berita);
        }
    }
}
