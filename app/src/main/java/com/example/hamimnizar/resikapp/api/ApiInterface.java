package com.example.hamimnizar.resikapp.api;

import com.example.hamimnizar.resikapp.model.sampah.Place;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {

//    @FormUrlEncoded
//    @POST("user/login.php")
//    Call<Login> postLogin(
//            @FieldMap HashMap<String, String> params
//    );

    @GET("districts")
    Call <Place> getPlace();


}