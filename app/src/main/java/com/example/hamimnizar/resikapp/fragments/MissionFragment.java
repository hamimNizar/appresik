package com.example.hamimnizar.resikapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.hamimnizar.resikapp.AdapterMission;
import com.example.hamimnizar.resikapp.ModelM;
import com.example.hamimnizar.resikapp.R;

import java.util.ArrayList;
import java.util.List;

public class MissionFragment extends Fragment {

    public RecyclerView recyclerView;
    AdapterMission adapterM;
    List<ModelM> modelms;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_mission, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerviewm);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getContext());
        recyclerView.setLayoutManager(layoutManager);
        ((LinearLayoutManager) layoutManager).setOrientation(LinearLayoutManager.VERTICAL);

        modelms = new ArrayList<>();
        modelms.add(new ModelM(R.drawable.home_ic_maths, "Buang sampah sembarangan", "30", "70", "done"));
        modelms.add(new ModelM(R.drawable.home_ic_maths, "Buang sampah sembarangan", "30", "70", "done"));
        modelms.add(new ModelM(R.drawable.home_ic_maths, "Buang sampah sembarangan", "30", "70", "done"));
        modelms.add(new ModelM(R.drawable.home_ic_maths, "Buang sampah sembarangan", "30", "70", "done"));
        modelms.add(new ModelM(R.drawable.home_ic_maths, "Buang sampah sembarangan", "30", "70", "done"));
        modelms.add(new ModelM(R.drawable.home_ic_maths, "Buang sampah sembarangan", "30", "70", "done"));
        modelms.add(new ModelM(R.drawable.home_ic_maths, "Buang sampah sembarangan", "30", "70", "done"));
        modelms.add(new ModelM(R.drawable.home_ic_maths, "Buang sampah sembarangan", "30", "70", "done"));
        modelms.add(new ModelM(R.drawable.home_ic_maths, "Buang sampah sembarangan", "30", "70", "done"));


        adapterM = new AdapterMission(this, modelms);
        recyclerView.setAdapter(adapterM);

        return view;
    }
}
