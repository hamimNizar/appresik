package com.example.hamimnizar.resikapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.hamimnizar.resikapp.AdapterNewsH;
import com.example.hamimnizar.resikapp.AdapterNewsV;
import com.example.hamimnizar.resikapp.Model;
import com.example.hamimnizar.resikapp.R;

import java.util.ArrayList;
import java.util.List;

public class NewsFragment extends Fragment {

    public RecyclerView recyclerViewH, recyclerViewV;
    AdapterNewsH adapterH;
    AdapterNewsV adapterV;
    List<Model> models;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_news, container, false);

        recyclerViewH = (RecyclerView) view.findViewById(R.id.recyclerviewH);
        recyclerViewV = (RecyclerView) view.findViewById(R.id.recyclerviewV);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getContext());
        recyclerViewH.setLayoutManager(layoutManager);
        ((LinearLayoutManager) layoutManager).setOrientation(LinearLayoutManager.HORIZONTAL);
        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(this.getContext());
        recyclerViewV.setLayoutManager(layoutManager2);
        ((LinearLayoutManager) layoutManager2).setOrientation(LinearLayoutManager.VERTICAL);

        models = new ArrayList<>();
        models.add(new Model(R.drawable.resikvektor,"Njajal sek Njajal"));
        models.add(new Model(R.drawable.resikvektor,"jsjdjshkdhasjdha"));
        models.add(new Model(R.drawable.resikvektor,"NjajaldasNjajal"));
        models.add(new Model(R.drawable.resikvektor,"Njajasdasdadaajal"));

        adapterH = new AdapterNewsH(this, models);
        recyclerViewH.setAdapter(adapterH);
        adapterV = new AdapterNewsV(this, models);
        recyclerViewV.setAdapter(adapterV);

        return view;
    }
}
