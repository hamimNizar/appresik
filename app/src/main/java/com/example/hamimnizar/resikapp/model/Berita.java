package com.example.hamimnizar.resikapp.model;

public class Berita {
    private String judul;
    private String tanggal;
    private String isi;
    private int image;

    public Berita(String judul, String tanggal, String isi, int image) {
        this.judul = judul;
        this.tanggal = tanggal;
        this.isi = isi;
        this.image = image;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
