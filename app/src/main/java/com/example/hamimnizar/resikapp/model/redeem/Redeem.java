package com.example.hamimnizar.resikapp.model.redeem;

public class Redeem {
    private String title;
    private int img_barang;
    private String harga;

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImg_barang() {
        return img_barang;
    }

    public void setImg_barang(int img_barang) {
        this.img_barang = img_barang;
    }
}
