package com.example.hamimnizar.resikapp.redeem;

import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;

import com.example.hamimnizar.resikapp.R;
import com.example.hamimnizar.resikapp.api.ApiClient;
import com.example.hamimnizar.resikapp.api.ApiInterface;
import com.example.hamimnizar.resikapp.model.redeem.Redeem;
import com.example.hamimnizar.resikapp.model.sampah.Data_Place;
import com.example.hamimnizar.resikapp.model.sampah.Place;
import com.example.hamimnizar.resikapp.sampah.ListPlaceActivity;
import com.example.hamimnizar.resikapp.sampah.PlaceAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RedeemActivity extends AppCompatActivity {

    private RecyclerView rv_redeem;
    private RedeemAdapter redeemAdapter;

    ApiInterface apiInterface;
    Call<Place> call_redeem;
    List<Data_Place> listRedeem;

    int[] barang = {R.drawable.boneka, R.drawable.bunga, R.drawable.celengan, R.drawable.pensil, R.drawable.pot, R.drawable.tisu};
    String[] title_barang={"Boneka","Hiasan Bunga", "Celengan", "Tempat Pensil", "Pot Bunga","Tempat Tisu"};
    String[] poin={"1000","1500", "1200", "3000", "650","300"};


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_redeem);

        listRedeem = new ArrayList<>();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        rv_redeem = findViewById(R.id.rv_redeem);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        rv_redeem.setLayoutManager(mLayoutManager);
        rv_redeem.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10),
                true));

        ArrayList<Redeem> detailItembaruModels = prepareData();

        redeemAdapter = new RedeemAdapter(this, detailItembaruModels);
        rv_redeem.setAdapter(redeemAdapter);
        rv_redeem.getAdapter().notifyDataSetChanged();

//        getData();

    }

    private ArrayList<Redeem> prepareData(){

        ArrayList<Redeem> android_version = new ArrayList<>();
        for(int i=0;i<title_barang.length;i++){
            Redeem androidVersion = new Redeem();
            androidVersion.setTitle(title_barang[i]);
            androidVersion.setHarga(poin[i]);
            androidVersion.setImg_barang(barang[i]);
            android_version.add(androidVersion);
        }
        return android_version;
    }

    private void getData() {
        call_redeem = apiInterface.getPlace();

        call_redeem.enqueue(new Callback<Place>() {
            @Override
            public void onResponse(Call<Place> call, Response<Place> response) {
                //List Items
                listRedeem = response.body().getData();
//                redeemAdapter = new RedeemAdapter(RedeemActivity.this, listRedeem);
                rv_redeem.setAdapter(redeemAdapter);
                rv_redeem.getAdapter().notifyDataSetChanged();

            }
            @Override
            public void onFailure(Call<Place> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }
    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
