package com.example.hamimnizar.resikapp.redeem;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.hamimnizar.resikapp.R;
import com.example.hamimnizar.resikapp.model.redeem.Redeem;
import com.example.hamimnizar.resikapp.model.sampah.Data_Place;

import java.util.ArrayList;
import java.util.List;


public class RedeemAdapter extends RecyclerView.Adapter<RedeemAdapter.ItemRowHolder> {
//    List<Data_Place> dataItems;
    ArrayList<Redeem> dataItems;
    private Activity activity;

    public RedeemAdapter(Activity activity, ArrayList<Redeem> dataItems){
        this.dataItems = dataItems;
        this.activity = activity;
    }

    @NonNull
    @Override
    public ItemRowHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_redeem, viewGroup, false);
        ItemRowHolder mh = new ItemRowHolder(view);
        return mh;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemRowHolder itemRowHolder, int i) {
        final Redeem detailmenuModel = dataItems.get(i);

        itemRowHolder.title.setText(detailmenuModel.getTitle());
        Glide.with(activity).load(detailmenuModel.getImg_barang()).into(itemRowHolder.img_icon);
        itemRowHolder.btn_redeem.setText(detailmenuModel.getHarga());

        itemRowHolder.btn_redeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(activity, OrderActivity.class);
//                intent.putExtra("i\d_item",detailmenuModel.getIdItem());
//                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != dataItems ? dataItems.size() : 0);
    }

    public class ItemRowHolder extends RecyclerView.ViewHolder {
//        LinearLayout line_place;
        protected TextView title,poin;
        protected ImageView img_icon;
        protected Button btn_redeem;

        public ItemRowHolder(@NonNull View itemView) {
            super(itemView);

            this.btn_redeem = itemView.findViewById(R.id.btn_redeem);
            this.title = itemView.findViewById(R.id.tvNama);
            this.img_icon = itemView.findViewById(R.id.ivRedeem);
        }
    }
}
