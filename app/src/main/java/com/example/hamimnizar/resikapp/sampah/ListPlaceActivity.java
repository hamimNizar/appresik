package com.example.hamimnizar.resikapp.sampah;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.hamimnizar.resikapp.R;
import com.example.hamimnizar.resikapp.api.ApiClient;
import com.example.hamimnizar.resikapp.api.ApiInterface;
import com.example.hamimnizar.resikapp.model.sampah.Data_Place;
import com.example.hamimnizar.resikapp.model.sampah.Place;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListPlaceActivity extends AppCompatActivity {

    private RecyclerView rv_place;
    private PlaceAdapter placeAdapter;

    ApiInterface apiInterface;
    Call<Place> call_place;
    List<Data_Place> listPlace;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_place);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("List Location");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        listPlace = new ArrayList<>();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        rv_place = findViewById(R.id.rv_place);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        rv_place.setLayoutManager(mLayoutManager);

        getData();
    }

    private void getData() {
        call_place = apiInterface.getPlace();

        call_place.enqueue(new Callback<Place>() {
            @Override
            public void onResponse(Call<Place> call, Response<Place> response) {
                //List Items
                listPlace = response.body().getData();
                placeAdapter = new PlaceAdapter(ListPlaceActivity.this, listPlace);
                rv_place.setAdapter(placeAdapter);
                rv_place.getAdapter().notifyDataSetChanged();

            }
            @Override
            public void onFailure(Call<Place> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
