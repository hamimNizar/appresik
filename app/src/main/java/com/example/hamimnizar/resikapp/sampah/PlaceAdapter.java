package com.example.hamimnizar.resikapp.sampah;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.hamimnizar.resikapp.R;
import com.example.hamimnizar.resikapp.model.sampah.Data_Place;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class PlaceAdapter extends RecyclerView.Adapter<PlaceAdapter.ItemRowHolder> {
    List<Data_Place> dataItems;
    private Activity activity;

    public PlaceAdapter(Activity activity, List<Data_Place> dataItems){
        this.dataItems = dataItems;
        this.activity = activity;
    }

    @NonNull
    @Override
    public ItemRowHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_place, viewGroup, false);
        ItemRowHolder mh = new ItemRowHolder(view);
        return mh;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemRowHolder itemRowHolder, int i) {
        final Data_Place detailmenuModel = dataItems.get(i);

        itemRowHolder.title.setText(detailmenuModel.getName());
        itemRowHolder.type.setText(detailmenuModel.getInformation());
        itemRowHolder.address.setText(detailmenuModel.getAddress());
//        Glide.with(activity).load(detailmenuModel.getPhoto()).into(itemRowHolder.img_place);
        Glide.with(activity).load(detailmenuModel.getPhoto()).into(itemRowHolder.img_icon);

        itemRowHolder.line_place.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(activity, OrderActivity.class);
//                intent.putExtra("i\d_item",detailmenuModel.getIdItem());
//                activity.startActivity(intent);
                Intent intent = new Intent(activity, DetailActivity.class);
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != dataItems ? dataItems.size() : 0);
    }

    public class ItemRowHolder extends RecyclerView.ViewHolder {
        LinearLayout line_place;
        protected TextView title,type,address;
        protected ImageView img_place,img_icon;

        public ItemRowHolder(@NonNull View itemView) {
            super(itemView);

            this.line_place = itemView.findViewById(R.id.llPlace);
            this.title = itemView.findViewById(R.id.tvNama);
            this.type = itemView.findViewById(R.id.tv_type);
            this.address = itemView.findViewById(R.id.tv_address);
            this.img_place = itemView.findViewById(R.id.ivPlace);
            this.img_icon = itemView.findViewById(R.id.icon_type);
        }
    }
}
